package com.springca.svs;

import javax.json.JsonObject;

public class UserInfo {
    String authorityKeyId;
    String certSn;
    String userType;
    String additionalField1;
    String orgName;
    String orgCode;
    String businessNum;
    String socialSecurityNum;
    String taxRegistrationCode;
    String socialCreditCode;
    String personalName;
    String idCardNum;
    String validFrom;
    String validTo;
    String authLogId;

    public UserInfo(JsonObject userInfo) {
        authorityKeyId = userInfo.getString("authorityKeyId", null);
        certSn = userInfo.getString("certSn", null);
        userType = userInfo.getString("userType", null);
        additionalField1 = userInfo.getString("additionalField1", null);
        orgName = userInfo.getString("orgName", null);
        orgCode = userInfo.getString("orgCode", null);
        businessNum = userInfo.getString("businessNum", null);
        socialSecurityNum = userInfo.getString("socialSecurityNum", null);
        taxRegistrationCode = userInfo.getString("taxRegistrationCode", null);
        socialCreditCode = userInfo.getString("socialCreditCode", null);
        personalName = userInfo.getString("personalName", null);
        idCardNum = userInfo.getString("idCardNum", null);
        validFrom = userInfo.getString("validFrom", null);
        validTo = userInfo.getString("validTo", null);
        authLogId = userInfo.getString("authLogId", null);
    }

    public String getAuthorityKeyId() {
        return authorityKeyId;
    }

    public String getCertSn() {
        return certSn;
    }

    public String getUserType() {
        return userType;
    }

    public String getAdditionalField1() {
        return additionalField1;
    }

    public String getOrgName() {
        return orgName;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public String getBusinessNum() {
        return businessNum;
    }

    public String getSocialSecurityNum() {
        return socialSecurityNum;
    }

    public String getTaxRegistrationCode() {
        return taxRegistrationCode;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public String getPersonalName() {
        return personalName;
    }

    public String getIdCardNum() {
        return idCardNum;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public String getAuthLogId() {
        return authLogId;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "certSn='" + certSn + '\'' +
                ", userType='" + userType + '\'' +
                ", orgName='" + orgName + '\'' +
                ", orgCode='" + orgCode + '\'' +
                ", businessNum='" + businessNum + '\'' +
                ", socialSecurityNum='" + socialSecurityNum + '\'' +
                ", taxRegistrationCode='" + taxRegistrationCode + '\'' +
                ", socialCreditCode='" + socialCreditCode + '\'' +
                ", personalName='" + personalName + '\'' +
                ", idCardNum='" + idCardNum + '\'' +
                ", validFrom='" + validFrom + '\'' +
                ", validTo='" + validTo + '\'' +
                ", authLogId='" + authLogId + '\'' +
                ", additionalField1='" + additionalField1 + '\'' +
                '}';
    }
}
