package com.springca.svs;

public class AppAuth {
    private String appId;
    private String appSecret;

    public AppAuth(String appId, String appSecret) {
        this.appId = appId;
        this.appSecret = appSecret;
    }

    public String getAppId() {
        return appId;
    }

    public String getAppSecret() {
        return appSecret;
    }
}
