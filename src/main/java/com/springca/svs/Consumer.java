package com.springca.svs;

import com.springca.helper.HttpHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@SuppressWarnings("unused")
public class Consumer {
    private static Logger log = LoggerFactory.getLogger(Consumer.class);
    private String serviceUrl;
    private AppAuth auth;

    public Consumer(String serviceUrl) {
        this.serviceUrl = serviceUrl.trim().replaceFirst("/$", "");
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public AppAuth getAuth() {
        return auth;
    }

    public void setAuth(AppAuth auth) {
        this.auth = auth;
    }

    public String getAuthUrl(String signInUrl) {
        try {
            return serviceUrl + "/sso/certLogin?appId=" + URLEncoder.encode(auth.getAppId(), "UTF-8") + "&appUrl=" + URLEncoder.encode(signInUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new Error("this can not be happened, but...", e);
        }
    }

    public UserInfo retrieve(String ticket) throws IOException {
        // 1. 组装请求
        JsonObjectBuilder reqDataBuilder = Json.createObjectBuilder()
                .add("ticket", ticket);
        appendAuthInfo(reqDataBuilder);
        // 2. 发送请求
        JsonObject repData = HttpHelper.postSignedJson(serviceUrl + "/sso/getTicket.json", reqDataBuilder.build(), auth.getAppSecret());
        // 3. 获取响应
        log.trace("接口返回json:\t" + repData.toString());
        // 4. 根据响应数据填充EstampDocument实例并返回
        if (repData.getBoolean("isOk")) {
            JsonObject jsonUserInfo = repData.getJsonObject("userInfo");
            return new UserInfo(jsonUserInfo);
        } else {
            throw new IOException("接口调用错误：" + repData.getString("message"));
        }
    }

    protected void appendAuthInfo(JsonObjectBuilder reqDataBuilder) {
        reqDataBuilder
                .add("appId", auth.getAppId())
                .add("timestamp", System.currentTimeMillis());
    }

    public String getVersion() {
        // 第一位主版本与第二位副版本与SVS-REST-API版本一致，第三位修订版本用于修订版本的发布
        return "2.0.0";
    }

}
