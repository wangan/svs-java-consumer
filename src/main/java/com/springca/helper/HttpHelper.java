package com.springca.helper;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;


@SuppressWarnings("unused")
public class HttpHelper {
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private static final String CHARSET = "UTF-8";

    public static JsonObject postSignedJson(String requestUri, JsonObject reqData, String appSecret) throws IOException {
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            String jsonString = reqData.toString();
            byte[] jsonBytes = jsonString.getBytes(CHARSET);
            Key signingKey = new SecretKeySpec(appSecret.getBytes(CHARSET), HMAC_SHA1_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(jsonBytes);
            HttpPost httpPost = new HttpPost(requestUri);
            httpPost.addHeader("Accept", "application/json; charset=utf-8");
            httpPost.addHeader("Content-Signature", "HMAC-SHA1 " + Base64.encodeBase64String(rawHmac).replaceAll("\\s",""));
            ByteArrayEntity reqEntity = new ByteArrayEntity(jsonBytes, ContentType.APPLICATION_JSON);
            httpPost.setEntity(reqEntity);
            debug(httpPost, jsonString);
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
                return Json.createReader(httpResponse.getEntity().getContent()).readObject();
            } else {
                throw new IOException(httpResponse.getStatusLine().toString() + " ERROR\nResponseText=" + IOUtils.toString(httpResponse.getEntity().getContent()));
            }
        } catch (NoSuchAlgorithmException e) {
            throw new Error("this can not be happened, but...", e);
        } catch (InvalidKeyException e) {
            throw new Error("this can not be happened, but...", e);
        }
    }

    private static void debug(HttpPost httpPost, String content) {
        if (!System.getProperty("estamp.client.debug", "false").equals("true")) {
            System.out.println("REQUEST...");
            System.out.println(httpPost.getRequestLine().toString());
            for (Header header : httpPost.getAllHeaders()) {
                System.out.println(header.getName() + " : " + header.getValue());
            }
            System.out.println(content);
        }
    }
}
