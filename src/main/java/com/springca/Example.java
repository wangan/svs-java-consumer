package com.springca;

import com.springca.svs.AppAuth;
import com.springca.svs.Consumer;
import com.springca.svs.UserInfo;

import java.io.IOException;

public class Example {
    public static void main(String[] args) {
        Consumer consumer = new Consumer("http://cert.hzca.org.cn/warriors/svs");
        consumer.setAuth(new AppAuth("test", "05ba1e05d095dd85bc793f8ff80d0119"));
        String authUrl = consumer.getAuthUrl("https://www.baidu.com");
        System.out.println(authUrl);

        String ticket = "ST-1043-rpXeKrWxPonJOyZaoUQo";

        try {
            UserInfo userInfo = consumer.retrieve(ticket);
            System.out.println(userInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
